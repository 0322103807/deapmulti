import { initializeApp } from '@firebase/app';

const firebaseConfig = {
  apiKey: "AIzaSyDY98cAUwFwpwsENMGCnjaIqRdSo_sS0ho",
  authDomain: "parradevapp-ba8f8.firebaseapp.com",
  projectId: "parradevapp-ba8f8",
  storageBucket: "parradevapp-ba8f8.appspot.com",
  messagingSenderId: "989411192143",
  appId: "1:989411192143:web:ec21646614d45b2928293a"
};

const app = initializeApp(firebaseConfig);

export default app;
